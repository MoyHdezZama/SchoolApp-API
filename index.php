<?php
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

define("SPECIALCONSTANT", true);

$username = null;
$password = null;
$_username_ = "user";
$_password_ = "pass";

// mod_php
if (isset($_SERVER['PHP_AUTH_USER'])) {
    $username = $_SERVER['PHP_AUTH_USER'];
    $password = $_SERVER['PHP_AUTH_PW'];

// most other servers
} elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) {

        if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']),'basic')===0)
          list($username,$password) = explode(':',base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));

}


if (is_null($username)) {

    header('WWW-Authenticate: Basic realm="Binne Systems"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Text to send if user hits Cancel button';

    die();

} else {
    if(!(($username == $_username_)&&($password == $_password_))){
        die ('Acceso denegado');
    }
}



//$auth_realm = 'Binne Systems';

//require 'auth.php';

require 'app/libs/connect.php';
require 'app/routes/AlumnoController/LoginAlumnoController.php';
require 'app/routes/AlumnoController/HorarioAlumnoController.php';
require 'app/routes/AlumnoController/TareasAlumnoController.php';
require 'app/routes/AlumnoController/TareaRealizadaAlumnoController.php';
require 'app/routes/AlumnoController/ExamenesAlumnoController.php';
require 'app/routes/AlumnoController/MateriasAlumnoController.php';
require 'app/routes/AlumnoController/AnotarTareaAlumnoController.php';
require 'app/routes/ProfesorController/LoginProfesorController.php';
require 'app/routes/ProfesorController/HorarioProfesorController.php';
require 'app/routes/ProfesorController/AnotarTareaProfesorController.php';
require 'app/routes/ProfesorController/GruposProfesorController.php';
require 'app/routes/ProfesorController/ExamenesProfesorController.php';
require 'app/routes/ProfesorController/TareasProfesorController.php';
require 'app/routes/AlumnoController/EditarTareaAlumnoController.php';
require 'app/routes/ProfesorController/EditarTareaProfesorController.php';
require 'app/routes/ProfesorController/EliminarTareaProfesorController.php';
require 'app/routes/AlumnoController/EliminarTareaAlumnoController.php';
require 'app/routes/ProfesorController/CalificarTareaProfesorController.php';
require 'app/routes/ProfesorController/TareasCalificarProfesorController.php';
require 'app/routes/CambiarContrasenaController.php';
require 'app/routes/CambiarPalabraClaveController.php';
require 'app/routes/AlumnoController/SubirArchivoAlumnoController.php';
require 'app/routes/ProfesorController/ArchivosAlumnoProfesorController.php';
require 'app/routes/EnviarNotificacion.php';
require 'app/routes/AlumnoController/GuardarIdRegistroAlumno.php';
require 'app/routes/ProfesorController/GuardarIdRegistroTrabajador.php';
require 'app/routes/HorarioNotificacionController.php';
require 'app/routes/CerrarSesionController.php';
$app->run();