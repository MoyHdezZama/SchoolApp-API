<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/TareasAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$registro = $data["registro"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT tarea.id_tarea, tarea.titulo, tarea.descripcion, tarea.fecha_limite, tarea.estado_alumno, materia.nombre AS nombre_materia, tarea.FK_profesor AS profesor, tarea.estado, tarea.archivo_alumno, tarea.archivo_profesor FROM tarea INNER JOIN materia ON tarea.FK_materia = materia.id_materia WHERE tarea.FK_alumno = ? AND tarea.fecha_limite >= curdate() AND tarea.estado = 1 AND tarea.titulo <> 'Examen' ORDER BY tarea.fecha_limite /*DESC*/");
		$dbh->bindParam(1, $registro);
		$dbh->execute();
		$tareas = $dbh->fetchALL(PDO::FETCH_ASSOC);
		$connection = null;
		$tareas = array('tareas' => $tareas );

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($tareas));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/TareasAlumno/", function() use($app)
{
});

$app->delete("/TareasAlumno/:id", function($id) use($app)
{
});
