<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/AnotarTareaAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$FK_alumno = $data["fk_alumno"];
	$FK_materia = $data["fk_materia"];
	$titulo = $data["titulo"];
	$descripcion = $data["descripcion"];
	$fecha_limite = $data["fecha_limite"];
	$archivo_alumno = $data["archivo_alumno"];

	try{

		$connection = getConnection();
		$fk_grupo = $connection->prepare("SELECT fk_grupo FROM grupo_alumno WHERE fk_alumno = ?");
		$fk_grupo->bindParam(1,$fk_alumno);
		$fk_grupo->execute();
		$fk_grupo = $fk_grupo->fetch(PDO::FETCH_ASSOC);

		$dbh = $connection->prepare("INSERT INTO tarea VALUES(null, 103, ?, ?, ?, ?, ?, 0, curdate(), ?, -1, '', ?, 1)");
		$dbh->bindParam(1, $FK_alumno);
		$dbh->bindParam(2, $FK_materia);
		$dbh->bindParam(3, $fk_grupo);
		$dbh->bindParam(4, $titulo);
		$dbh->bindParam(5, $descripcion);
		$dbh->bindParam(6, $fecha_limite);
		$dbh->bindParam(7, $archivo_alumno);
		$dbh->execute();
		$connection = null;

		$success = array(
			"code" => 200
			);

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/AnotarTareaAlumno/", function() use($app)
{
});

$app->delete("/TareaRealizada/:id", function($id) use($app)
{
});
