<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/EditarTareaAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$id_tarea = $data["id_tarea"];
	$FK_materia = $data["fk_materia"];
	$titulo = $data["titulo"];
	$descripcion = $data["descripcion"];
	$fecha_limite = $data["fecha_limite"];

	try{

		$connection = getConnection();

		$dbh = $connection->prepare("UPDATE tarea SET FK_materia = ?, titulo = ?, descripcion = ?, fecha_limite = ? WHERE id_tarea = ?");
		$dbh->bindParam(1, $FK_materia);
		$dbh->bindParam(2, $titulo);
		$dbh->bindParam(3, $descripcion);
		$dbh->bindParam(4, $fecha_limite);
		$dbh->bindParam(5, $id_tarea);
		$dbh->execute();
		$connection = null;

		$success = array(
			"code" => 200
			);

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/EditarTareaAlumno/", function() use($app)
{
});

$app->delete("/EditarTareaAlumno/:id", function($id) use($app)
{
});
