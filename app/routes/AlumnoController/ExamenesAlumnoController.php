<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/ExamenesAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$registro = $data["registro"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT tarea.id_tarea, tarea.titulo, tarea.descripcion, tarea.fecha_limite, materia.nombre AS nombre_materia, tarea.estado FROM tarea INNER JOIN materia ON tarea.FK_materia = materia.id_materia WHERE tarea.FK_alumno = ? AND tarea.fecha_limite > curdate() AND tarea.titulo = 'Examen' AND tarea.estado = 1");
		$dbh->bindParam(1, $registro);
		$dbh->execute();
		$examen = $dbh->fetchALL(PDO::FETCH_ASSOC);
		$connection = null;
		$examen = array("examenes"  => $examen);

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($examen));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/ExamenesAlumno/", function() use($app)
{
});

$app->delete("/ExamenesAlumno/:id", function($id) use($app)
{
});
		