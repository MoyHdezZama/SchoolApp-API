<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/EliminarTareaAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$id_tarea = $data["id_tarea"];

	try{

		$connection = getConnection();

		$dbh = $connection->prepare("DELETE FROM tarea WHERE id_tarea = ? AND FK_profesor = 103");
		$dbh->bindParam(1, $id_tarea);
		$dbh->execute();
		$connection = null;


		$success = array("code" => 200);


		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/EliminarTareaAlumno/", function() use($app)
{
});

$app->delete("/EliminarTareaAlumno/:id", function($id) use($app)
{
});
