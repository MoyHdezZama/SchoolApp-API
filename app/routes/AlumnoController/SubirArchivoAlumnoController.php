<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/SubirArchivoAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$id_tarea = $data["id_tarea"];
	$archivo_alumno = $data["archivo_alumno"];

	try{

		$connection = getConnection();

		$dbh = $connection->prepare("UPDATE tarea SET  archivo_alumno = ? WHERE id_tarea = ?");
		$dbh->bindParam(1, $archivo_alumno);
		$dbh->bindParam(2, $id_tarea);
		$dbh->execute();
		$connection = null;

		$success = array("code" => 200);


		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/SubirArchivoAlumno/", function() use($app)
{
});

$app->delete("/SubirArchivoAlumno/:id", function($id) use($app)
{
});
