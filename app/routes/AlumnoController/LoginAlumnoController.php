<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/LoginAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$registro = $data["registro"];
	$password = $data["password"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT alumno.nombre, alumno.apellido_paterno, alumno.apellido_materno, area_carrera.nombre AS nombre_carrera, alumno.estado FROM alumno INNER JOIN area_carrera ON alumno.carrera = area_carrera.id_area WHERE alumno.registro = ? AND alumno.password = MD5(?) AND alumno.estado = TRUE");
		$dbh->bindParam(1, $registro);
		$dbh->bindParam(2, $password);
		$dbh->execute();
		$usuario = $dbh->fetch(PDO::FETCH_ASSOC);
		$connection = null;
		if ($usuario==null) {
			$usuario = array(
				"nombre" => "",
				"apellido_paterno" => "",
				"apellido_materno" => "",
				"nombre_carrera" => 0,
				"estado" => 0
				);
		}

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($usuario));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/LoginAlumno/", function() use($app)
{
});

$app->delete("/LoginAlumno/:id", function($id) use($app)
{
});
