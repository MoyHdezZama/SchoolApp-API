<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/TareaRealizadaAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$EstadoAlumno = $data["estado_alumno"];
	$ID = $data["id_tarea"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE tarea SET estado_alumno = ? WHERE id_tarea = ?");
		$dbh->bindParam(1, $EstadoAlumno);
		$dbh->bindParam(2, $ID);
		//print_r($dbh);
		$dbh->execute();
		//$usuario = "true";
		$connection = null;

		$success = array("code" => 200);


		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/TareaRealizadaAlumno/", function() use($app)
{
});

$app->delete("/TareaRealizadaAlumno/:id", function($id) use($app)
{
});
