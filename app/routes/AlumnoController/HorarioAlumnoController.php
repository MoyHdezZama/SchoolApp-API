<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/HorarioAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$registro = $data["registro"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT horario.dia AS dia_semana, horario.hora, grupo.grupo, grupo.grado, salon.edificio, salon.nombre AS nombre_salon, materia.nombre AS nombre_materia, horario.estado FROM horario INNER JOIN salon ON horario.FK_salon = salon.id_salon INNER JOIN materia ON horario.FK_materia = materia.id_materia INNER JOIN grupo ON horario.FK_grupo = grupo.id_grupo INNER JOIN grupo_alumno ON grupo.id_grupo = grupo_alumno.fk_grupo WHERE grupo_alumno.FK_alumno = ? AND horario.estado = 1 ORDER BY horario.dia, horario.hora");
		$dbh->bindParam(1, $registro);
		$dbh->execute();
		$horario = $dbh->fetchALL(PDO::FETCH_ASSOC);
		$connection = null;
		$horario = array("horario" => $horario);
		/*if ($horario==null) {
			$horario = array(
				"dia" => 0,
				"hora" => 0,
				"grupo" => 	"",
				"edificio" => "",
				"nombre_salon" => "",
				"nombre_materia" => "",
				"estado" => 0
				);
		}*/
		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($horario));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/HorarioAlumno/", function() use($app)
{
});

$app->delete("/HorarioAlumno/:id", function($id) use($app)
{
});
	