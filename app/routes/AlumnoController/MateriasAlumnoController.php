<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/MateriasAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$registro = $data["registro"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT DISTINCT materia.nombre AS nombre_materia, materia.id_materia, materia.estado FROM horario INNER JOIN materia ON materia.id_materia = horario.FK_materia INNER JOIN grupo_alumno ON grupo_alumno.fk_grupo = horario.FK_grupo WHERE grupo_alumno.FK_alumno = ? AND materia.estado = 1");
		$dbh->bindParam(1, $registro);
		$dbh->execute();
		$materia = $dbh->fetchALL(PDO::FETCH_ASSOC);
		$connection = null;
		$materia = array("materias" => $materia);
		/*if ($materia==null) {
			$materia = array(
				"nombre" => "",
				"id_materia" => 0,
				"estado" => 0
				);
		}*/

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($materia));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/MateriasAlumno/", function() use($app)
{
});

$app->delete("/MateriasAlumno/:id", function($id) use($app)
{
});
?>
	