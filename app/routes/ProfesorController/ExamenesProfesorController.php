<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/ExamenesProfesor/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$nomina = $data["nomina"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT DISTINCT tarea.titulo, tarea.descripcion, tarea.fecha_limite, materia.nombre AS nombre_materia, grupo.grado, grupo.grupo, grupo.subgrupo , tarea.estado FROM tarea INNER JOIN materia ON tarea.FK_materia = materia.id_materia INNER JOIN grupo ON tarea.FK_grupo = grupo.id_grupo WHERE tarea.FK_profesor = ? AND tarea.fecha_limite > curdate() AND tarea.titulo = 'Examen' AND tarea.estado = 1");
		$dbh->bindParam(1, $nomina);
		$dbh->execute();
		$examen = $dbh->fetchALL(PDO::FETCH_ASSOC);
		$connection = null;

			$examen = array(
				"examenes" => $examen
				);

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($examen));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/ExamenesProfesor/", function() use($app)
{
});

$app->delete("/ExamenesProfesor/:id", function($id) use($app)
{
});
