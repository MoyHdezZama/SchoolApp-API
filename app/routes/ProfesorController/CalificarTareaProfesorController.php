<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/CalificarTareaProfesor/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$calificacion = $data["calificacion"];
	$id_tarea = $data["id_tarea"];

	try{

		$connection = getConnection();

		$dbh = $connection->prepare("UPDATE tarea SET calificacion = ? WHERE id_tarea = ?");
		$dbh->bindParam(1, $calificacion);
		$dbh->bindParam(2, $id_tarea);
		$dbh->execute();
		$connection = null;

		$success = array("code" => 200);


		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/CalificarTareaProfesor/", function() use($app)
{
});

$app->delete("/CalificarTareaProfesor/:id", function($id) use($app)
{
});
