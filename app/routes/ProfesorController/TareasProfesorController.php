<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/TareasProfesor/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$nomina = $data["nomina"];
	$grupo = $data["grupo"];

	try{

		$connection = getConnection();
		if($grupo==0){
			$dbh = $connection->prepare("SELECT DISTINCT materia.nombre AS nombre_materia, grupo.grado, grupo.grupo, grupo.subgrupo, tarea.titulo, tarea.descripcion, tarea.fecha_limite, tarea.archivo_profesor FROM tarea INNER JOIN materia On materia.id_materia = tarea.FK_materia INNER JOIN grupo ON grupo.id_grupo = tarea.FK_grupo WHERE tarea.FK_profesor = ? AND tarea.titulo <> 'Examen' ORDER BY tarea.fecha_limite ASC");
		}else{
			$dbh = $connection->prepare("SELECT DISTINCT materia.nombre AS nombre_materia, grupo.grado, grupo.grupo, grupo.subgrupo, tarea.titulo, tarea.descripcion, tarea.fecha_limite , tarea.archivo_profesor FROM tarea INNER JOIN materia On materia.id_materia = tarea.FK_materia INNER JOIN grupo ON grupo.id_grupo = tarea.FK_grupo WHERE tarea.FK_profesor = ? AND tarea.titulo <> 'Examen' AND tarea.fk_grupo = $grupo ORDER BY tarea.fecha_limite ASC");

		}
		$dbh->bindParam(1, $nomina);
		$dbh->execute();
		$usuario = $dbh->fetchALL(PDO::FETCH_ASSOC);
		$connection = null;
			$usuario = array(
				"tareas" => $usuario
				);
		

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($usuario));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/TareasProfesor/", function() use($app)
{
});

$app->delete("/TareasProfesor/:id", function($id) use($app)
{
});
