<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/TareasCalificarProfesor/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$nombre_materia = $data["nombre_materia"];
	$titulo = $data["titulo"];
	$descripcion = $data["descripcion"];
	$fecha_limite = $data["fecha_limite"];
	$grado = $data["grado"];
	$grupo = $data["grupo"];
	$subgrupo = $data["subgrupo"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT id_tarea, FK_alumno AS fk_alumno, calificacion, CONCAT(alumno.nombre,' ',alumno.apellido_paterno,' ',alumno.apellido_materno) AS nombre_alumno FROM tarea INNER JOIN alumno on tarea.FK_alumno = alumno.registro WHERE FK_materia = (SELECT materia.id_materia FROM materia WHERE materia.nombre = ?) AND titulo = ? AND descripcion = ? AND fecha_limite = ? AND FK_grupo = (SELECT grupo.id_grupo FROM grupo WHERE grupo.grado = ? AND grupo.grupo = ? AND grupo.subgrupo = ? ORDER BY FK_alumno DESC)");

		$dbh->bindParam(1, $nombre_materia);
		$dbh->bindParam(2, $titulo);
		$dbh->bindParam(3, $descripcion);
		$dbh->bindParam(4, $fecha_limite);
		$dbh->bindParam(5, $grado);
		$dbh->bindParam(6, $grupo);
		$dbh->bindParam(7, $subgrupo);
		$dbh->execute();
		$usuario = $dbh->fetchALL(PDO::FETCH_ASSOC);
		$connection = null;
			$usuario = array(
				"tareas" => $usuario
				);

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($usuario));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/TareasCalificarProfesor/", function() use($app)
{
});

$app->delete("/TareasCalificarProfesor/:id", function($id) use($app)
{
});
