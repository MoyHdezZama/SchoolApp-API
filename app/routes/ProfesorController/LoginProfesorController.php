<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/LoginProfesor/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$nomina = $data["nomina"];
	$password = $data["password"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT trabajador.nombre, trabajador.apellido_paterno, trabajador.apellido_materno, area_carrera.nombre AS nombre_area, trabajador.estado FROM trabajador INNER JOIN area_carrera ON trabajador.FK_area = area_carrera.id_area WHERE trabajador.nomina = ? AND trabajador.password = MD5(?) AND trabajador.estado = TRUE");
		$dbh->bindParam(1, $nomina);
		$dbh->bindParam(2, $password);
		$dbh->execute();
		$usuario = $dbh->fetch(PDO::FETCH_ASSOC);
		$connection = null;
		if ($usuario==null) {
			$usuario = array(
				"nombre" => "",
				"apellido_paterno" => "",
				"apellido_materno" => "",
				"nombre_area" => 0,
				"estado" => 0
				);
		}

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($usuario));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/LoginProfesor/", function() use($app)
{
});

$app->delete("/LoginProfesor/:id", function($id) use($app)
{
});
