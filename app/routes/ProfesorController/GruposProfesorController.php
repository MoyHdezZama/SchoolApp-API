<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/GruposProfesor/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$nomina = $data["nomina"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT DISTINCT grupo.grado, grupo.grupo, grupo.subgrupo, grupo.id_grupo FROM grupo INNER JOIN horario ON horario.FK_grupo = grupo.id_grupo WHERE horario.FK_trabajador = ?");
		$dbh->bindParam(1, $nomina);
		$dbh->execute();
		$grupos = $dbh->fetchALL(PDO::FETCH_ASSOC);
		$connection = null;

			$grupos = array(
				"grupos" => $grupos,
				);

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($grupos));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/GruposProfesor/", function() use($app)
{
});

$app->delete("/GruposProfesor/:id", function($id) use($app)
{
});
?>
