<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/GuardarIdRegistroTrabajador/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$Nomina = $data["registro"];
	$RegId = $data["idReg"];


	try{

		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE trabajador SET idReg=? WHERE nomina=?");
		$dbh->bindParam(1,$RegId);
		$dbh->bindParam(2,$Nomina);


		$dbh->execute();
		$connection = null;

		$success = array("code" => 200);


		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/EnviarAvisoAlumno/", function() use($app)
{
});

$app->delete("/EnviarAvisoAlumno/:id", function($id) use($app)
{
});
