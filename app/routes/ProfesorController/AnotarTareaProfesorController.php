<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/AnotarTareaProfesor/", function() use($app)
{
	// API access key from Google API's Console
	define('API_ACCESS_KEY','AIzaSyDn6B3pTAAaHip3edvGFWLmwSnZKD3PYRM');

	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();
	$grupo = $data["grupo"];
	$nomina = $data["nomina"];
	$titulo = $data["titulo"];
	$descripcion = $data["descripcion"];
	$fecha_limite = $data["fecha_limite"];
	$archivo_profesor = $data["archivo_profesor"];
	$archivo_alumno = $data["archivo_alumno"];


	try{

		$connection = getConnection();
		$registros = $connection->prepare("SELECT fk_alumno FROM grupo_alumno WHERE fk_grupo = ?");
		$registros->bindParam(1,$grupo);
		$registros->execute();
		$registros = $registros->fetchALL(PDO::FETCH_ASSOC);
		$count = count($registros);

		$idRegs = $connection->prepare("SELECT alumno.idReg FROM alumno INNER JOIN grupo_alumno ON alumno.registro = grupo_alumno.fk_alumno WHERE fk_grupo = ? AND alumno.idReg <> ''");
		$idRegs->bindParam(1,$grupo);
		$idRegs->execute();
		$idRegs = $idRegs->fetchALL(PDO::FETCH_ASSOC);
		$countIdRegs = count($idRegs);

		$fk_materia = $connection->prepare("SELECT fk_materia FROM horario WHERE fk_grupo = ? AND fk_trabajador = ?");
		$fk_materia->bindParam(1,$grupo);
		$fk_materia->bindParam(2,$nomina);
		$fk_materia->execute();
		$fk_materia = $fk_materia->fetch(PDO::FETCH_ASSOC);

		$nombreMateria = $connection->prepare("SELECT nombre FROM materia WHERE id_materia = ?");
		$nombreMateria->bindParam(1, $fk_materia["fk_materia"]);
		$nombreMateria->execute();
		$nombreMateria = $nombreMateria->fetch(PDO::FETCH_ASSOC);

		for ($i=0; $i < $count; $i++) {
			$x = $connection->prepare("INSERT INTO tarea VALUES(null, ?, ?, ?, ?, ?, ?, 0, curdate(), ?, -1, ?, ?, 1)");
			$x->bindParam(1,$nomina);
			$x->bindParam(2, $registros[$i]['fk_alumno']);
			$x->bindParam(3, $fk_materia["fk_materia"]);
			$x->bindParam(4, $grupo);
			$x->bindParam(5, $titulo);
			$x->bindParam(6, $descripcion);
			$x->bindParam(7, $fecha_limite);
			$x->bindParam(8, $archivo_profesor);
			$x->bindParam(9, $archivo_alumno);
			$x->execute();
		}
		for($i = 0; $i< $countIdRegs; $i++){

			$registrationIds=array($idRegs[$i]['idReg']);

			if(strcasecmp ($titulo,"Examen")==0){
				$msg = array
				(
					'message' 	=> $descripcion,
					'title'		=> 'Nuevo examen de '.$nombreMateria['nombre'],
					'subtitle'	=> 'CETI',
					'tickerText'	=> 'Tienes un nueva Tarea',
					'vibrate'	=> 1,
					'sound'		=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
				);

			}else{
				$msg = array
				(
					'message' 	=> $titulo,
					'title'		=> 'Nueva Tarea de '.$nombreMateria['nombre'],
					'subtitle'	=> 'CETI',
					'tickerText'	=> 'Tienes un nueva Tarea',
					'vibrate'	=> 1,
					'sound'		=> 1,
					'largeIcon'	=> 'large_icon',
					'smallIcon'	=> 'small_icon'
				);

			}

			$fields = array
			(
				'registration_ids' 	=> $registrationIds,
				'data'			=> $msg
			);

			$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);


			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );

		}

		$connection = null;

		$success = array("code" => 200);


		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/AnotarTareaAlumno/", function() use($app)
{
});

$app->delete("/TareaRealizada/:id", function($id) use($app)
{
});
