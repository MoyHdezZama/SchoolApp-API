<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/ArchivosAlumno/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$titulo = $data["titulo"];
	$descripcion = $data["descripcion"];
	$nombre_materia = $data["nombre_materia"];
	$grado = $data["grado"];
	$grupo = $data["grupo"];
	$subgrupo = $data["subgrupo"];

	try{

		$connection = getConnection();
		$dbh = $connection->prepare("SELECT CONCAT(alumno.registro,'-',alumno.nombre,' ',alumno.apellido_paterno,' ',alumno.apellido_materno) as nombre, tarea.archivo_alumno FROM tarea INNER JOIN alumno ON tarea.FK_alumno = alumno.registro WHERE tarea.titulo = ? AND tarea.descripcion = ? AND tarea.FK_materia = (SELECT materia.id_materia FROM materia WHERE materia.nombre = ?) AND FK_grupo = (SELECT grupo.id_grupo FROM grupo WHERE grupo.grado = ? AND grupo.grupo = ? AND grupo.subgrupo = ?) AND tarea.archivo_alumno <> '1' AND tarea.archivo_alumno <> ''");
		$dbh->bindParam(1, $titulo);
		$dbh->bindParam(2, $descripcion);
		$dbh->bindParam(3, $nombre_materia);
		$dbh->bindParam(4, $grado);
		$dbh->bindParam(5, $grupo);
		$dbh->bindParam(6, $subgrupo);
		$dbh->execute();
		$usuario = $dbh->fetchALL(PDO::FETCH_ASSOC);
		$connection = null;

		$usuario = array(
			"archivos" => (($usuarios != null) ? $usuarios : array())
			);

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($usuario));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/ArchivosAlumno/", function() use($app)
{
});

$app->delete("/ArchivosAlumno/:id", function($id) use($app)
{
});
	