<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/EditarTareaProfesor/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$tituloNuevo = $data["tituloNuevo"];
	$descripcionNuevo = $data["descripcionNuevo"];
	$fecha_limiteNuevo = $data["fecha_limiteNuevo"];
	$nombre_materia = $data["nombre_materia"];
	$titulo = $data["titulo"];
	$descripcion = $data["descripcion"];
	$fecha_limite = $data["fecha_limite"];
	$FK_profesor = $data["fk_profesor"];

	try{

		$connection = getConnection();

		$dbh = $connection->prepare("UPDATE tarea SET titulo = ?, descripcion = ?, fecha_limite = ? WHERE FK_materia = (SELECT materia.id_materia FROM materia WHERE materia.nombre = ?) AND titulo = ? AND descripcion = ? AND fecha_limite = ? AND FK_profesor = ?");
		$dbh->bindParam(1, $tituloNuevo);
		$dbh->bindParam(2, $descripcionNuevo);
		$dbh->bindParam(3, $fecha_limiteNuevo);
		$dbh->bindParam(4, $nombre_materia);
		$dbh->bindParam(5, $titulo);
		$dbh->bindParam(6, $descripcion);
		$dbh->bindParam(7, $fecha_limite);
		$dbh->bindParam(8, $FK_profesor);
		$dbh->execute();
		$connection = null;

		$success = array("code" => 200);


		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/EditarTareaProfesor/", function() use($app)
{
});

$app->delete("/EditarTareaProfesor/:id", function($id) use($app)
{
});
