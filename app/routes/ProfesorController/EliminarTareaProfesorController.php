<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/EliminarTareaProfesor/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$nombre_materia = $data["nombre_materia"];
	$titulo = $data["titulo"];
	$descripcion = $data["descripcion"];
	$fecha_limite = $data["fecha_limite"];
	$FK_profesor = $data["subgrupo"];

	try{

		$connection = getConnection();

		$dbh = $connection->prepare("DELETE FROM tarea WHERE FK_materia = (SELECT materia.id_materia FROM materia WHERE materia.nombre = ?) AND titulo = ? AND descripcion = ? AND fecha_limite = ? AND FK_profesor = ?");
		$dbh->bindParam(1, $nombre_materia);
		$dbh->bindParam(2, $titulo);
		$dbh->bindParam(3, $descripcion);
		$dbh->bindParam(4, $fecha_limite);
		$dbh->bindParam(5, $FK_profesor);
		$dbh->execute();
		$connection = null;

		$success = array("code" => 200);


		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/EliminarTareaProfesor/", function() use($app)
{
});

$app->delete("/EliminarTareaProfesor/:id", function($id) use($app)
{
});
