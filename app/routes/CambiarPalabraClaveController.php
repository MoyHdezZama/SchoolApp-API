<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/CambiarPalabraClave/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$id = $data["id"];
	$palabra_clave = $data["palabra_clave"];
	$password = $data["password"];

	try{

		$connection = getConnection();
		print_r($id);
		if($id<10001){
			$dbh1 = $connection->prepare("UPDATE trabajador SET trabajador.palabra_clave = ? WHERE trabajador.nomina = ? AND trabajador.password = MD5(?)");
			$dbh1->bindParam(1, $palabra_clave);
			$dbh1->bindParam(2, $id);
			$dbh1->bindParam(3, $password);
			$dbh1->execute();
			$trabajador = $connection->prepare("SELECT trabajador.estado FROM trabajador WHERE trabajador.nomina = ? AND trabajador.password = MD5(?)");
			$trabajador->bindParam(1, $id);
			$trabajador->bindParam(2, $password);
			$trabajador->execute();
			$usuario = $trabajador->fetch(PDO::FETCH_ASSOC);
			$connection = null;
			if ($usuario==null) {
				$usuario = array(
					"nomina" => 0
					);
			}
		}else{
			$dbh2 = $connection->prepare("UPDATE alumno SET alumno.palabra_clave = ? WHERE alumno.registro = ? AND alumno.password = MD5(?)");
			$dbh2->bindParam(1, $palabra_clave);
			$dbh2->bindParam(2, $id);
			$dbh2->bindParam(3, $password);
			$dbh2->execute();
			$alumno = $connection->prepare("SELECT alumno.estado FROM alumno WHERE alumno.registro = ? AND alumno.password = MD5(?)");
			$alumno->bindParam(1, $id);
			$alumno->bindParam(2, $password);
			$alumno->execute();
			$usuario = $alumno->fetch(PDO::FETCH_ASSOC);
			$connection = null;
			if ($usuario==null) {
				$usuario = array(
					"estado" => 0
					);
			}
		}

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($usuario));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/CambiarPalabraClave/", function() use($app)
{
});

$app->delete("/CambiarPalabraClave/:id", function($id) use($app)
{
});
