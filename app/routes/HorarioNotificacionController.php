<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/HorarioNotificacion/", function() use($app)
{
	define('API_ACCESS_KEY','AIzaSyDn6B3pTAAaHip3edvGFWLmwSnZKD3PYRM');

	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();



	$id = $data["id"];
	$dia = $data["dia"];
	$hora = $data["hora"];

	try{

		$connection = getConnection();
		if($id <= 10000){
			$dbh = $connection->prepare("SELECT materia.nombre AS nombre_materia, CONCAT(salon.edificio, '-',salon.nombre) AS salon, CONCAT(grupo.grado, grupo.grupo, grupo.subgrupo) AS grupo, idReg FROM horario INNER JOIN materia ON horario.FK_materia = materia.id_materia INNER JOIN grupo ON horario.FK_grupo = grupo.id_grupo INNER JOIN salon ON horario.FK_salon = salon.id_salon INNER JOIN trabajador ON horario.FK_trabajador = trabajador.nomina WHERE horario.dia = ? AND horario.hora = ? AND horario.fk_trabajador = ?");
			$dbh->bindParam(1, $dia);
			$dbh->bindParam(2, $hora);
			$dbh->bindParam(3, $id);
			$dbh->execute();
			$horario = $dbh->fetch(PDO::FETCH_ASSOC);
			$connection = null;
			if ($horario==null) {
				$horario = array(
					"nombre_materia" => "",
					"salon" => "",
					"grupo" => 	"",
					"idReg" => ""
					);
			}
			$registrationIds = array($horario['idReg']);
			$nombre_materia = $horario['nombre_materia'];
			$salon = $horario['salon'];
			$grupo = $horario['grupo'];
		}else{
			$dbh = $connection->prepare("SELECT materia.nombre AS nombre_materia, CONCAT(salon.edificio, '-',salon.nombre) AS salon, CONCAT(grupo.grado,grupo.grupo,grupo.subgrupo) AS grupo, idReg FROM horario INNER JOIN materia ON horario.FK_materia = materia.id_materia INNER JOIN grupo ON horario.FK_grupo = grupo.id_grupo INNER JOIN salon ON horario.FK_salon = salon.id_salon INNER JOIN grupo_alumno ON grupo.id_grupo = grupo_alumno.fk_grupo INNER JOIN alumno on grupo_alumno.fk_alumno = alumno.registro WHERE horario.dia = ? AND horario.hora = ? AND grupo_alumno.fk_alumno = ?");
			$dbh->bindParam(1, $dia);
			$dbh->bindParam(2, $hora);
			$dbh->bindParam(3, $id);
			$dbh->execute();
			$horario = $dbh->fetch(PDO::FETCH_ASSOC);
			$connection = null;
			if ($horario==null) {
				$horario = array(
					"nombre_materia" => "",
					"salon" => "",
					"grupo" => 	"",
					"idReg" =>	""
					);
			}
			$registrationIds =array($horario['idReg']);
			$nombre_materia = $horario['nombre_materia'];
			$salon = $horario['salon'];
			$grupo = $horario['grupo'];
		}

		$msg = array
			(
				'message' 	=> $nombre_materia.', '.$salon.', '.$grupo,
				'title'		=> 'Tienes clase',
				'subtitle'	=> 'Este es un subtitulo',
				'tickerText'	=> 'Clase',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			);

			$fields = array
			(
				'registration_ids' 	=> $registrationIds,
				'data'			=> $msg
			);

			$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);

		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		curl_close( $ch );
		/*$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($horario));*/
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/HorarioNotificacion/", function() use($app)
{
});

$app->delete("/HorarioNotificacion/:id", function($id) use($app)
{
});
