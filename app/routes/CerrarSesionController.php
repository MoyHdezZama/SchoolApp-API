<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/CerrarSesion/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$registro = $data["registro"];

	try{

		if($registro >= 10001)
		{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE alumno set idReg='' WHERE registro = ?");
		$dbh->bindParam(1, $registro);
		$dbh->execute();

		$connection = null;
		}
		else
		{
		$connection = getConnection();
		$dbh = $connection->prepare("UPDATE trabajador set idReg='' WHERE nomina = ?");
		$dbh->bindParam(1, $registro);
		$dbh->execute();

		$connection = null;
		}

		$success = array("code" => 200);


		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($success));


	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});
			