<?php
if(!defined("SPECIALCONSTANT")) die("Acceso denegado");

$app->post("/CambiarContrasena/", function() use($app)
{
	$data = json_decode( $app->request()->getBody() ) ?: $app->request->params();

	$id = $data["id"];
	$palabra_clave = $data["palabra_clave"];
	$password = $data["password"];

	try{

		$connection = getConnection();
			print_r("alumno");
		if($id<=10000){
			print_r("trabajador");
			$dbh1 = $connection->prepare("UPDATE trabajador SET trabajador.password = MD5(?) WHERE trabajador.nomina = ? AND trabajador.palabra_clave = ?");
			$dbh1->bindParam(1, $password);
			$dbh1->bindParam(2, $id);
			$dbh1->bindParam(3, $palabra_clave);
			$dbh1->execute();
			$trabajador = $connection->prepare("SELECT trabajador.estado FROM trabajador WHERE trabajador.nomina = ? AND trabajador.palabra_clave = ?");
			$trabajador->bindParam(1, $id);
			$trabajador->bindParam(2, $palabra_clave);
			$trabajador->execute();
			$usuario = $trabajador->fetch(PDO::FETCH_ASSOC);
			$connection = null;
			if ($usuario==null) {
				$usuario = array(
					"nomina" => 0
					);
			}
		}else{
			print_r("alumno");
			$dbh2 = $connection->prepare("UPDATE alumno SET alumno.password = MD5(?) WHERE alumno.registro = ? AND alumno.palabra_clave = ?");
			$dbh2->bindParam(1, $password);
			$dbh2->bindParam(2, $id);
			$dbh2->bindParam(3, $palabra_clave);
			$dbh2->execute();
			$alumno = $connection->prepare("SELECT alumno.estado FROM alumno WHERE alumno.registro = ? AND alumno.palabra_clave = ?");
			$alumno->bindParam(1, $id);
			$alumno->bindParam(2, $palabra_clave);
			$alumno->execute();
			$usuario = $alumno->fetch(PDO::FETCH_ASSOC);
			$connection = null;
			if ($usuario==null) {
				$usuario = array(
					"estado" => 0
					);
			}
		}

		$app->response->headers->set("Content-type", "application/json");
		$app->response->status(200);
		$app->response->body(json_encode($usuario));
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
});

$app->put("/CambiarContrasena/", function() use($app)
{
});

$app->delete("/CambiarContrasena/:id", function($id) use($app)
{
});
