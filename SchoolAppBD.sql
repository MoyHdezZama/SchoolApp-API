
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-02-2017 a las 00:23:41
-- Versión del servidor: 10.0.28-MariaDB
-- Versión de PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `u146717901_bd1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
  `registro` int(11) NOT NULL AUTO_INCREMENT,
  `carrera` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido_paterno` varchar(30) NOT NULL,
  `apellido_materno` varchar(30) NOT NULL,
  `mac` varchar(17) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(50) NOT NULL,
  `palabra_clave` varchar(50) NOT NULL,
  `idReg` text NOT NULL,
  PRIMARY KEY (`registro`),
  KEY `carrera` (`carrera`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12300498 ;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`registro`, `carrera`, `nombre`, `apellido_paterno`, `apellido_materno`, `mac`, `estado`, `password`, `palabra_clave`, `idReg`) VALUES
(12300023, 8, 'Armando Dicarlo', 'Arceo', 'Gomez', '1', 1, '83a1a1c1da46c00a08618d063a6abf11', 'shot', ''),
(12300156, 8, 'Diego', 'Franco', 'Negro', '0', 1, '6d0007e52f7afb7d5a0650b0ffb8a4d1', 'diego', ''),
(12300239, 8, 'Moises', 'Hernandez', 'Zamarripa', '0', 1, 'bf6acd226798c581b29e318038d67e11', 'moy', 'enDRZgXfSXs:APA91bHJ2Lv3sw0SdF2oUjl0yOfTSgppaZat9kSgkYTHYAeJw-lPetYLT_njQxcgUlilAUQB3Ssb87s5eZyvf5-hOPucYSa4DAtLm4VwfFCdSeh48R9Nwv_lZVCIkyWUtyH6wX9QWjpt'),
(12300266, 8, 'Fabian', 'Jimenez', 'Villegas', '0', 1, '412577135376f07f93840c65175a597d', 'fabis', ''),
(12300379, 9, 'Edgar Ivan', 'Padilla', 'Allende', '0', 1, '242c3d4de7eb843333cb8653348637bd', 'chino', 'eQFT2xbRIAE:APA91bEvnFPZfVL3DKg7jZ0JsyazwQmGXk-5KjHlI4bcwh_0jUnbFLMbsG0opwQAb-vdXErIXL207ToP01AeWdI-mP0qpt7fNAArge-LyG0BWYs1htzsvd5FxA6cuDbaNb6mIEsAHkOY'),
(12300392, 8, 'Jose Raul', 'Partida', 'Iñiguez', '0', 1, 'a7a752e0e035c086fe685518c6547d05', 'rulo', ''),
(12300443, 8, 'Sergio', 'Robles', 'Delgadillo', '0', 1, 'd99808348c50f5d4b24457cf577edbde', 'checo', 'eCuQ-XgRuqg:APA91bEJ4IOB3OmDmXE5AWkFO2GadLsrNQb4p5AGWzdZByALMizJLNkB8pknUZUhYsL2rf6S6slNhs5heC2umr_cZ9AmheIuC0q9gGBufMwhkph3EtDmbuC-CAzAQH8iIir7WlzPix3P'),
(12300453, 8, 'Eunice', 'Martin del Campo', 'Castañeda', '0', 1, 'c9199c53c2f321d9aee75aa99d4b74cc', 'Castañeda', ''),
(12300454, 8, 'Brian', 'Rubio', 'Sabe que', '0', 1, '4d236810821e8e83a025f2a83ea31820', 'Sabe que', ''),
(12300491, 8, 'Luis Alfonso', 'Santillan', 'Prieto', '52', 1, '460fffd26a22c2c2a9a5fc6b2a59eb58', 'Prieto', ''),
(12300494, 8, 'David', 'Hernandez', 'Zamarripa', '25', 1, '464e07afc9e46359fb480839150595c5', 'Zamarripa', 'cy8NnHl-ZPg:APA91bEstm6dilvhpPW54fw-FFl1dueqkWrdfNOAGWd9JI0ZZe7169oVeiIsPb6ZX5SBtsis0UJDguNkBEC2G8_U7yRiCCLyMuUkMaAw9MhOZqWBn0lWcMBgi_a7obp73PUmxScgxTTb'),
(11300539, 8, 'Sebastian', 'Lara', 'Agraz', '5', 1, 'b69eb4ca4b4ae19e8e4f2e3129da7fd3', 'Agraz', ''),
(12300172, 8, 'Diego', 'Garcia', 'Medina', '0', 1, '4fb845c67d91bcb3178498fc6fe1fedc', 'Medina', 'f4fnLtqEf2M:APA91bE6LHxxOz0rXI7aQD_OEcV0o1MrJp7hx0icKS4_P0XH6Lpul4oINTMvdCGUBjDeqNuYoLc5y_7lVOpf5k56Gt0lAq0oJU7PysckNifMMgE9woVQbLSx8OebpohyixMYBKg8skls'),
(12300497, 9, 'moy el guapo', 'sexy', 'papi brat pitt 2', '0', 0, 'd9ea69415bd08e8a19c7245a9d753362', 'papi brat pitt 2', ''),
(12300496, 8, 'Marisa', 'Quintero', 'Covarubias', '0', 1, '987ef1b615f8eb11b999caa7f959a7bc', 'Covarubias', 'con9YxGUZFk:APA91bF1QIJlMbS1wnLTrpRxg47KwCoNzqeAHQbPLuZmp3AxHa03DokSJ4tyDENBadcRXBc2CqgYaUS3Ra7yXg6_a57Kl9_AtkVHDzv9YiuthVYWyqEdkdtDZVvs9sRRz6V4nPKQxxEX');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_carrera`
--

CREATE TABLE IF NOT EXISTS `area_carrera` (
  `id_area` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_area`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Volcado de datos para la tabla `area_carrera`
--

INSERT INTO `area_carrera` (`id_area`, `nombre`, `estado`) VALUES
(3, 'Administrativas', 1),
(4, 'Basicas', 1),
(8, 'Desarrollo de Software', 1),
(9, 'Construccion', 1),
(10, 'Control Automatico e Instrumen', 1),
(11, 'Desarrollo Electronico', 1),
(12, 'Electronica y Comunicaciones', 1),
(13, 'Electromecanica', 1),
(14, 'Maquinas y Herramientas', 1),
(15, 'Mecanica Automotriz', 1),
(16, 'Quimico en Alimentos', 1),
(17, 'Quimico en Farmacos', 1),
(18, 'Quimico Industrial', 1),
(7, 'General', 1),
(20, 'cadMaestro', 1),
(21, 'cadCoordinador', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envia_mensaje`
--

CREATE TABLE IF NOT EXISTS `envia_mensaje` (
  `id_envia_mensajeTT` int(11) NOT NULL AUTO_INCREMENT,
  `FK_emisor` int(11) NOT NULL,
  `FK_receptor` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id_envia_mensajeTT`),
  KEY `FK_emisor` (`FK_emisor`),
  KEY `FK_receptor` (`FK_receptor`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envia_mensajeaa`
--

CREATE TABLE IF NOT EXISTS `envia_mensajeaa` (
  `id_envia_mensajeAA` int(11) NOT NULL AUTO_INCREMENT,
  `FK_emisor` int(11) NOT NULL,
  `FK_receptor` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `texto` text NOT NULL,
  PRIMARY KEY (`id_envia_mensajeAA`),
  KEY `FK_emisor` (`FK_emisor`,`FK_receptor`),
  KEY `FK_receptor` (`FK_receptor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `envia_mensajeaa`
--

INSERT INTO `envia_mensajeaa` (`id_envia_mensajeAA`, `FK_emisor`, `FK_receptor`, `fecha`, `hora`, `texto`) VALUES
(1, 12300379, 12300156, '0000-00-00', '00:00:00', 'que onda'),
(2, 12300453, 12300453, '0000-00-00', '00:00:00', 'Hence n we I ft b a free information I free about jajajaja no jajaja no j\nGarrido'),
(3, 12300156, 12300379, '0000-00-00', '00:00:00', 'Yyhhyh');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envia_mensajeta`
--

CREATE TABLE IF NOT EXISTS `envia_mensajeta` (
  `id_envia_mensaje_ta` int(11) NOT NULL AUTO_INCREMENT,
  `registro_alumno` int(11) NOT NULL,
  `nomina_trabajador` int(11) NOT NULL,
  `texto` text NOT NULL,
  `envia` tinyint(1) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  PRIMARY KEY (`id_envia_mensaje_ta`),
  KEY `registro_alumno` (`registro_alumno`,`nomina_trabajador`),
  KEY `nomina_trabajador` (`nomina_trabajador`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Volcado de datos para la tabla `envia_mensajeta`
--

INSERT INTO `envia_mensajeta` (`id_envia_mensaje_ta`, `registro_alumno`, `nomina_trabajador`, `texto`, `envia`, `fecha`, `hora`) VALUES
(1, 12300156, 101, 'hola alumno', 1, '2016-05-30', '14:20:44'),
(2, 12300156, 101, 'Hola profe ', 2, '2016-05-30', '14:24:27'),
(3, 12300156, 101, 'Yolo ', 2, '2016-05-30', '14:39:25'),
(4, 12300156, 101, 'Profe cual es la tarea? ', 2, '2016-05-31', '10:51:17'),
(5, 12300156, 101, 'no hay tarea :)', 1, '2016-05-31', '10:51:57'),
(6, 12300156, 101, 'Ooh muy bien gracias ', 2, '2016-05-31', '10:52:24'),
(7, 12300156, 105, 'primer mensaje', 1, '2016-05-31', '12:10:10'),
(8, 12300156, 105, 'siguiente mensaje', 1, '2016-05-31', '12:12:56'),
(9, 12300156, 105, 'Hola ', 2, '2016-05-31', '12:13:53'),
(10, 12300156, 115, 'Holiwis', 2, '2016-06-07', '14:27:52'),
(11, 12300156, 115, 'Hola', 2, '2016-06-07', '14:29:02'),
(12, 12300156, 115, 'hola diego', 1, '2016-06-07', '15:54:56'),
(13, 12300156, 115, 'Hola señor directo del plantel colomos ', 2, '2016-06-07', '15:55:25'),
(14, 12300379, 115, 'hola perro\n', 2, '2016-06-07', '16:04:19'),
(15, 12300156, 115, 'Hola dir', 2, '2016-06-07', '16:06:52'),
(16, 12300156, 101, 'Hola profe ', 2, '2016-06-07', '16:29:36'),
(17, 12300156, 119, 'Hola nueva persona ', 2, '2016-06-07', '16:30:14'),
(18, 12300156, 119, 'Hola niña ', 2, '2016-06-07', '16:57:21'),
(19, 12300156, 115, 'oli', 1, '2016-06-07', '16:58:16'),
(20, 12300156, 119, 'Hola Diana ', 2, '2016-06-07', '16:58:25'),
(21, 12300156, 115, 'Hahahaha', 2, '2016-06-07', '17:33:14'),
(22, 12300156, 115, 'Hola', 2, '2016-06-07', '17:48:00'),
(23, 12300156, 115, 'hola', 1, '2016-06-07', '17:53:34'),
(24, 12300156, 102, 'Hola profe ', 2, '2016-06-07', '17:59:12'),
(25, 12300156, 115, 'como estas?', 1, '2016-06-07', '18:12:19'),
(26, 12300156, 101, 'Eres un ñoño', 2, '2016-06-07', '18:19:46'),
(27, 12300156, 115, 'Fghhj', 2, '2016-06-07', '19:02:40'),
(28, 12300156, 115, 'hfhjk', 1, '2016-06-07', '19:07:17'),
(29, 12300156, 115, 'prro', 1, '2016-06-07', '19:07:53'),
(30, 12300156, 115, '', 2, '2016-06-07', '19:09:39'),
(31, 12300156, 115, 'Moy puto', 1, '2016-06-07', '19:10:25'),
(32, 12300156, 115, '2', 1, '2016-06-07', '19:10:45'),
(33, 12300156, 115, '', 2, '2016-06-07', '19:14:56'),
(34, 12300156, 115, 'Hola', 2, '2016-06-07', '19:37:07'),
(35, 12300156, 115, 'Hajshshdhs', 2, '2016-06-07', '19:49:26'),
(36, 12300156, 115, 'Hdhbg', 2, '2016-06-07', '19:59:54'),
(37, 12300156, 101, 'Y?', 2, '2016-06-07', '20:02:52'),
(38, 12300156, 110, 'Gfff', 2, '2016-06-07', '20:06:41'),
(39, 12300156, 115, 'Fahdhy', 2, '2016-06-07', '20:13:00'),
(40, 12300156, 115, 'Prueba1', 2, '2016-06-07', '20:54:30'),
(41, 12300156, 102, 'Hola ', 2, '2016-06-18', '17:52:41'),
(42, 12300156, 118, 'Hola', 2, '2016-06-18', '18:51:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envia_notificacion`
--

CREATE TABLE IF NOT EXISTS `envia_notificacion` (
  `id_envia_notificacionTT` int(11) NOT NULL AUTO_INCREMENT,
  `FK_emisor` int(11) NOT NULL,
  `FK_receptor` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ubicacion` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `titulo` varchar(20) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_envia_notificacionTT`),
  KEY `FK_emisor` (`FK_emisor`),
  KEY `FK_receptor` (`FK_receptor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Volcado de datos para la tabla `envia_notificacion`
--

INSERT INTO `envia_notificacion` (`id_envia_notificacionTT`, `FK_emisor`, `FK_receptor`, `fecha`, `hora`, `ubicacion`, `texto`, `titulo`, `estado`) VALUES
(3, 115, 99, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(4, 115, 116, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(5, 115, 101, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(6, 115, 102, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(7, 115, 103, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(8, 115, 104, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(9, 115, 105, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(10, 115, 106, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(11, 115, 107, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(12, 115, 108, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(13, 115, 109, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(14, 115, 110, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(15, 115, 111, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(16, 115, 115, '2016-06-17', '00:00:00', 'null', 'Junta a las 2', 'junta', 1),
(17, 115, 115, '2016-06-10', '00:00:00', 'null', 'app', 'Test', 1),
(18, 115, 123, '2016-06-10', '00:00:00', 'null', 'app', 'Test', 1),
(19, 115, 115, '2016-06-13', '16:00:00', 'F', 'Aplicaciones ', 'Test', 1),
(20, 115, 115, '2016-06-13', '00:00:00', 'null', 'ejemplo ', 'ejemplo ', 1),
(21, 115, 115, '2016-06-14', '00:00:00', 'null', 'proyecto ', 'proyecto', 1),
(22, 115, 115, '2016-06-23', '00:00:00', 'null', 'prueba', 'prueba', 1),
(23, 115, 115, '2016-06-16', '18:10:00', 'A', 'prueba ', 'prueba', 1),
(24, 115, 115, '2016-06-17', '00:00:00', 'null', 'gg', 'g', 1),
(25, 115, 115, '2016-06-17', '05:40:00', 'A', 'ggg', 'ggh', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envia_notificacionta`
--

CREATE TABLE IF NOT EXISTS `envia_notificacionta` (
  `id_envia_notificacionTA` int(11) NOT NULL AUTO_INCREMENT,
  `FK_emisor` int(11) NOT NULL,
  `FK_receptor` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ubicacion` varchar(150) NOT NULL,
  `texto` text NOT NULL,
  `titulo` varchar(20) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_envia_notificacionTA`),
  KEY `FK_emisor` (`FK_emisor`,`FK_receptor`),
  KEY `FK_receptor` (`FK_receptor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Volcado de datos para la tabla `envia_notificacionta`
--

INSERT INTO `envia_notificacionta` (`id_envia_notificacionTA`, `FK_emisor`, `FK_receptor`, `fecha`, `hora`, `ubicacion`, `texto`, `titulo`, `estado`) VALUES
(1, 115, 12300266, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(2, 115, 12300379, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(3, 115, 12300392, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(4, 115, 12300443, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(5, 115, 12300453, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(6, 115, 12300454, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(7, 115, 12300455, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(8, 115, 12300491, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(9, 115, 12300494, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(10, 115, 11300539, '2016-06-13', '00:00:00', 'null', 'El día lunes no hay clases muchachos, disfruten su puente.', 'Asueto', 1),
(11, 115, 12300156, '2016-06-14', '00:00:00', 'null', 'El martes se suspenden clases a las 2', 'Asueto', 1),
(12, 115, 12300156, '2016-06-08', '07:00:00', 'O', 'Asistir a la presentación de proyectos', 'Proyectos', 1),
(13, 115, 12300156, '2016-06-08', '00:00:00', 'null', 'No hay clases por motivo de junta ', 'Clases', 1),
(14, 115, 12300156, '2016-06-10', '00:00:00', 'null', 'El día viernes se entrega proyecto ', 'Entrega', 1),
(15, 115, 12300156, '2016-06-10', '12:00:00', 'B', 'Junta para posibles trabajos', 'Junta', 1),
(16, 115, 12300156, '2016-06-10', '00:00:00', 'null', 'se muestran los proyectos :)', 'Presentación ', 1),
(17, 115, 11300539, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(18, 115, 12300023, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(19, 115, 12300156, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(20, 115, 12300172, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(21, 115, 12300239, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(22, 115, 12300266, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(23, 115, 12300379, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(24, 115, 12300392, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(25, 115, 12300443, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(26, 115, 12300453, '2016-06-09', '02:00:00', 'J', 'hay una presentación de proyectos ', 'expo', 1),
(27, 115, 12300156, '2016-06-10', '00:00:00', 'null', '2', 'junta', 1),
(28, 115, 12300156, '2016-06-10', '00:00:00', 'null', 'se entrega proyecto el viernes', 'Proyecto ', 1),
(29, 115, 11300539, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(30, 115, 12300023, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(31, 115, 12300156, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(32, 115, 12300172, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(33, 115, 12300239, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(34, 115, 12300266, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(35, 115, 12300379, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(36, 115, 12300392, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(37, 115, 12300443, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(38, 115, 12300453, '2016-06-10', '13:00:00', 'F', 'se presentarán los proyectos ', 'presentación ', 1),
(39, 115, 12300156, '2016-06-17', '00:00:00', 'null', 'gggggg', 'hjhh', 1),
(40, 115, 12300156, '2016-06-13', '00:00:00', 'null', 'el lunes hay proyecto ', 'proyecto ', 1),
(41, 115, 12300156, '2016-06-11', '00:00:00', 'null', 'la app función:)', 'aplicaciones ', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE IF NOT EXISTS `grupo` (
  `id_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `fk_carrera` int(11) NOT NULL,
  `grado` int(11) NOT NULL,
  `grupo` varchar(1) NOT NULL,
  `subgrupo` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_grupo`),
  KEY `fk_carrera` (`fk_carrera`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`id_grupo`, `fk_carrera`, `grado`, `grupo`, `subgrupo`, `estado`) VALUES
(8, 8, 8, 'I', 1, 1),
(9, 8, 8, 'H', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_alumno`
--

CREATE TABLE IF NOT EXISTS `grupo_alumno` (
  `id_grupo_alumno` int(11) NOT NULL AUTO_INCREMENT,
  `fk_grupo` int(11) NOT NULL,
  `fk_alumno` int(11) NOT NULL,
  PRIMARY KEY (`id_grupo_alumno`),
  KEY `fk_grupo` (`fk_grupo`),
  KEY `fk_alumno` (`fk_alumno`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `grupo_alumno`
--

INSERT INTO `grupo_alumno` (`id_grupo_alumno`, `fk_grupo`, `fk_alumno`) VALUES
(1, 8, 12300023),
(2, 8, 12300156),
(3, 8, 12300239),
(4, 8, 12300266),
(5, 8, 12300379),
(6, 8, 12300392),
(7, 8, 12300443),
(8, 8, 12300453),
(9, 9, 12300491),
(10, 9, 12300455),
(11, 8, 11300539),
(12, 9, 12300494),
(13, 8, 12300172),
(14, 9, 12300496);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE IF NOT EXISTS `horario` (
  `id_horario` int(11) NOT NULL AUTO_INCREMENT,
  `FK_trabajador` int(11) NOT NULL,
  `FK_materia` int(11) NOT NULL,
  `FK_salon` int(11) NOT NULL,
  `FK_grupo` int(11) NOT NULL,
  `hora` int(2) NOT NULL,
  `dia` int(1) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_horario`),
  KEY `FK_trabajador` (`FK_trabajador`),
  KEY `FK_materia` (`FK_materia`),
  KEY `FK_salon` (`FK_salon`),
  KEY `FK_grupo` (`FK_grupo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=76 ;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`id_horario`, `FK_trabajador`, `FK_materia`, `FK_salon`, `FK_grupo`, `hora`, `dia`, `estado`) VALUES
(1, 102, 1, 5, 8, 1, 1, 1),
(2, 102, 1, 5, 8, 2, 1, 1),
(3, 107, 15, 6, 8, 3, 1, 1),
(4, 111, 17, 2, 8, 5, 1, 1),
(5, 101, 16, 21, 8, 6, 1, 1),
(6, 110, 14, 19, 8, 7, 1, 1),
(7, 110, 14, 19, 8, 8, 1, 1),
(8, 105, 19, 6, 8, 1, 2, 1),
(9, 105, 19, 6, 8, 2, 2, 1),
(10, 111, 17, 10, 8, 3, 2, 1),
(11, 108, 2, 11, 8, 4, 2, 1),
(12, 101, 16, 16, 8, 5, 2, 1),
(13, 101, 16, 16, 8, 6, 2, 1),
(14, 110, 14, 19, 8, 7, 2, 1),
(15, 106, 20, 7, 8, 1, 3, 1),
(16, 106, 20, 7, 8, 2, 3, 1),
(17, 108, 2, 11, 8, 3, 3, 1),
(18, 109, 18, 14, 8, 4, 3, 1),
(19, 111, 17, 17, 8, 5, 3, 1),
(20, 110, 14, 8, 8, 7, 3, 1),
(21, 110, 14, 8, 8, 8, 3, 1),
(22, 102, 1, 20, 8, 1, 4, 1),
(23, 102, 1, 20, 8, 2, 4, 1),
(24, 108, 2, 12, 8, 3, 4, 1),
(25, 109, 18, 15, 8, 4, 4, 1),
(26, 105, 19, 18, 8, 5, 4, 1),
(27, 105, 19, 18, 8, 6, 4, 1),
(28, 101, 16, 20, 8, 7, 4, 1),
(29, 101, 16, 20, 8, 8, 4, 1),
(30, 107, 15, 9, 8, 1, 5, 1),
(31, 107, 15, 9, 8, 2, 5, 1),
(32, 108, 2, 13, 8, 3, 5, 1),
(33, 111, 17, 12, 8, 4, 5, 1),
(34, 106, 20, 19, 8, 7, 5, 1),
(35, 106, 20, 19, 8, 8, 5, 1),
(36, 112, 21, 20, 9, 1, 1, 1),
(37, 112, 21, 20, 9, 2, 1, 1),
(38, 106, 20, 8, 9, 3, 1, 1),
(39, 106, 20, 8, 9, 4, 1, 1),
(40, 108, 2, 22, 9, 5, 1, 1),
(41, 114, 17, 23, 9, 6, 1, 1),
(42, 113, 23, 24, 9, 7, 1, 1),
(43, 113, 23, 24, 9, 8, 1, 1),
(44, 112, 21, 25, 9, 2, 2, 1),
(45, 112, 21, 25, 9, 1, 2, 1),
(46, 113, 22, 24, 9, 3, 2, 1),
(47, 113, 22, 24, 9, 4, 2, 1),
(48, 109, 18, 26, 9, 5, 2, 1),
(49, 114, 17, 26, 9, 6, 2, 1),
(50, 114, 17, 26, 9, 7, 2, 1),
(51, 110, 14, 19, 9, 8, 2, 1),
(52, 105, 19, 29, 9, 1, 3, 1),
(53, 105, 19, 29, 9, 2, 3, 1),
(54, 110, 14, 30, 9, 3, 3, 1),
(55, 110, 14, 30, 9, 4, 3, 1),
(56, 108, 2, 11, 9, 5, 3, 1),
(57, 114, 17, 28, 9, 6, 3, 1),
(58, 113, 22, 25, 9, 7, 3, 1),
(59, 113, 22, 25, 9, 8, 3, 1),
(60, 105, 19, 6, 9, 1, 4, 1),
(61, 105, 19, 6, 9, 2, 4, 1),
(62, 113, 23, 21, 9, 3, 4, 1),
(63, 113, 23, 21, 9, 4, 4, 1),
(64, 109, 18, 27, 9, 5, 4, 1),
(65, 109, 18, 27, 9, 6, 4, 1),
(66, 106, 20, 19, 9, 1, 5, 1),
(67, 106, 20, 19, 9, 2, 5, 1),
(68, 110, 14, 19, 9, 3, 5, 1),
(69, 110, 14, 19, 9, 4, 5, 1),
(70, 108, 2, 13, 9, 6, 5, 1),
(75, 112, 21, 16, 8, 6, 3, 1),
(74, 112, 21, 16, 8, 6, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imparte`
--

CREATE TABLE IF NOT EXISTS `imparte` (
  `id_imparte` int(11) NOT NULL AUTO_INCREMENT,
  `FK_trabajador` int(11) NOT NULL,
  `FK_materia` int(11) NOT NULL,
  PRIMARY KEY (`id_imparte`),
  KEY `FK_trabajador` (`FK_trabajador`),
  KEY `FK_materia` (`FK_materia`),
  KEY `FK_materia_2` (`FK_materia`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Volcado de datos para la tabla `imparte`
--

INSERT INTO `imparte` (`id_imparte`, `FK_trabajador`, `FK_materia`) VALUES
(1, 101, 16),
(2, 102, 4),
(3, 102, 14),
(4, 105, 19),
(5, 106, 20),
(6, 107, 15),
(7, 110, 14),
(8, 112, 21),
(9, 113, 22),
(10, 113, 23),
(11, 102, 1),
(12, 111, 17),
(13, 108, 2),
(14, 109, 18),
(15, 114, 17),
(21, 128, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

CREATE TABLE IF NOT EXISTS `materia` (
  `id_materia` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `fk_carrera` int(11) NOT NULL,
  `grado` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_materia`),
  KEY `fk_carrera` (`fk_carrera`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`id_materia`, `nombre`, `fk_carrera`, `grado`, `estado`) VALUES
(1, 'Proyecto de software', 8, 8, 1),
(2, 'Desarrollo de habilidades directivas', 3, 8, 1),
(4, 'Analisis de proyecto', 8, 8, 1),
(6, 'Administracion', 3, 8, 1),
(13, 'Gestion de Calidad', 3, 7, 1),
(14, 'Microcontroladores', 8, 8, 1),
(15, 'Desarrollo web', 8, 8, 1),
(16, 'Programacion aplicada avanzada', 8, 8, 1),
(17, 'Desarrollo empresarial', 3, 8, 1),
(18, 'Ingenieria economica', 3, 8, 1),
(19, 'Computacion industrial', 8, 8, 1),
(20, 'Introduccion a la robotica', 8, 8, 1),
(21, 'Seguridad en redes', 8, 8, 1),
(22, 'Tegnologia de redes wan', 8, 8, 1),
(23, 'Proyecto de redes', 8, 8, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salon`
--

CREATE TABLE IF NOT EXISTS `salon` (
  `id_salon` int(11) NOT NULL AUTO_INCREMENT,
  `edificio` varchar(10) NOT NULL,
  `nombre` varchar(10) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_salon`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `salon`
--

INSERT INTO `salon` (`id_salon`, `edificio`, `nombre`, `estado`) VALUES
(1, 'F', 's', 1),
(2, 'L', '101', 1),
(3, 'B', '106', 1),
(4, 'H', '215', 1),
(5, 'F', 'Lab-C', 1),
(6, 'F', 'SL3', 1),
(7, 'F', '203-A', 1),
(8, 'F', '203-B', 1),
(9, 'F', 'SL2', 1),
(10, 'G', '203', 1),
(11, 'B', '110', 1),
(12, 'B', '105', 1),
(13, 'B', '108', 1),
(14, 'B', 'I106', 1),
(15, 'B', 'IND', 1),
(16, 'F', 'Lab-B', 1),
(17, 'F', '202', 1),
(18, 'F', 'I2015', 1),
(19, 'F', 'LSDIG1', 1),
(20, 'F', 'Lab-E', 1),
(21, 'F', 'SL1', 1),
(22, 'B', '111', 1),
(23, 'B', '109', 1),
(24, 'F', 'LRED2', 1),
(25, 'F', 'LRED1', 1),
(26, 'L', '104', 1),
(27, 'L', '106', 1),
(28, 'L', '105', 1),
(29, 'F', '204-A', 1),
(30, 'F', '204-B', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

CREATE TABLE IF NOT EXISTS `tarea` (
  `id_tarea` int(11) NOT NULL AUTO_INCREMENT,
  `FK_profesor` int(11) NOT NULL,
  `FK_alumno` int(11) NOT NULL,
  `FK_materia` int(11) NOT NULL,
  `FK_grupo` int(11) NOT NULL,
  `titulo` varchar(30) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `estado_alumno` tinyint(1) NOT NULL,
  `fecha_subir` date NOT NULL,
  `fecha_limite` date NOT NULL,
  `calificacion` int(3) DEFAULT NULL,
  `archivo_profesor` text,
  `archivo_alumno` text,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_tarea`),
  KEY `FK_profesor` (`FK_profesor`),
  KEY `FK_alumno` (`FK_alumno`),
  KEY `FK_materia` (`FK_materia`),
  KEY `FK_grupo` (`FK_grupo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=301 ;

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`id_tarea`, `FK_profesor`, `FK_alumno`, `FK_materia`, `FK_grupo`, `titulo`, `descripcion`, `estado_alumno`, `fecha_subir`, `fecha_limite`, `calificacion`, `archivo_profesor`, `archivo_alumno`, `estado`) VALUES
(2, 101, 12300023, 16, 8, 'Exposición JPQL', 'que es?\nejemplo\netc', 0, '2016-06-07', '2016-06-16', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201682229_Banco de Preguntas.docx', '1', 1),
(3, 101, 12300156, 16, 8, 'Exposición JPQL', 'que es?\nejemplo\netc', 0, '2016-06-07', '2016-06-16', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201682229_Banco de Preguntas.docx', '1', 1),
(4, 101, 12300239, 16, 8, 'Exposición JPQL', 'que es?\nejemplo\netc', 1, '2016-06-07', '2016-06-16', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201682229_Banco de Preguntas.docx', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_75201682332_CRONOGRAMA.docx', 1),
(5, 101, 12300266, 16, 8, 'Exposición JPQL', 'que es?\nejemplo\netc', 0, '2016-06-07', '2016-06-16', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201682229_Banco de Preguntas.docx', '1', 1),
(6, 101, 12300379, 16, 8, 'Exposición JPQL', 'que es?\nejemplo\netc', 0, '2016-06-07', '2016-06-16', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201682229_Banco de Preguntas.docx', '1', 1),
(7, 101, 12300392, 16, 8, 'Exposición JPQL', 'que es?\nejemplo\netc', 0, '2016-06-07', '2016-06-16', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201682229_Banco de Preguntas.docx', '1', 1),
(8, 101, 12300443, 16, 8, 'Exposición JPQL', 'que es?\nejemplo\netc', 0, '2016-06-07', '2016-06-16', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201682229_Banco de Preguntas.docx', '1', 1),
(9, 101, 12300453, 16, 8, 'Exposición JPQL', 'que es?\nejemplo\netc', 0, '2016-06-07', '2016-06-16', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201682229_Banco de Preguntas.docx', '1', 1),
(10, 101, 11300539, 16, 8, 'Exposición JPQL', 'que es?\nejemplo\netc', 0, '2016-06-07', '2016-06-16', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201682229_Banco de Preguntas.docx', '1', 1),
(11, 105, 12300023, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 0, '2016-06-07', '2016-06-15', -1, '', '1', 1),
(12, 105, 12300156, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 0, '2016-06-07', '2016-06-15', -1, '', '1', 1),
(13, 105, 12300239, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 1, '2016-06-07', '2016-06-15', 100, '', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_752016102825_CRONOGRAMA.docx', 1),
(14, 105, 12300266, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 0, '2016-06-07', '2016-06-15', -1, '', '1', 1),
(15, 105, 12300379, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 0, '2016-06-07', '2016-06-15', -1, '', '1', 1),
(16, 105, 12300392, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 0, '2016-06-07', '2016-06-15', -1, '', '1', 1),
(17, 105, 12300443, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 0, '2016-06-07', '2016-06-15', -1, '', '1', 1),
(18, 105, 12300453, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 0, '2016-06-07', '2016-06-15', -1, '', '1', 1),
(19, 105, 11300539, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 0, '2016-06-07', '2016-06-15', -1, '', '1', 1),
(20, 105, 12300172, 19, 8, 'practicas', 'realizar práctica 5 y 6 esta semana', 0, '2016-06-07', '2016-06-15', -1, '', '1', 1),
(21, 105, 12300023, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(22, 105, 12300156, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(23, 105, 12300239, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(24, 105, 12300266, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(25, 105, 12300379, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(26, 105, 12300392, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(27, 105, 12300443, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(28, 105, 12300453, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(29, 105, 11300539, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(30, 105, 12300172, 19, 8, 'Examen', 'estudiar: plc\narduino\ny dac\n', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(31, 105, 12300491, 19, 9, 'Examen', 'estudiar todo lo que vimos en el parcial', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(32, 105, 12300455, 19, 9, 'Examen', 'estudiar todo lo que vimos en el parcial', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(33, 105, 12300494, 19, 9, 'Examen', 'estudiar todo lo que vimos en el parcial', 0, '2016-06-07', '2016-06-13', -1, '', NULL, 1),
(34, 105, 12300023, 19, 8, 'lo que usted quiera', 'hola', 0, '2016-06-07', '2016-06-09', 50, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(35, 105, 12300156, 19, 8, 'lo que usted quiera', 'hola', 0, '2016-06-07', '2016-06-09', 35, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(36, 105, 12300239, 19, 8, 'lo que usted quiera', 'hola', 1, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(37, 105, 12300266, 19, 8, 'lo que usted quiera', 'hola', 0, '2016-06-07', '2016-06-09', 25, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(38, 105, 12300379, 19, 8, 'lo que usted quiera', 'hola', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(39, 105, 12300392, 19, 8, 'lo que usted quiera', 'hola', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(40, 105, 12300443, 19, 8, 'lo que usted quiera', 'hola', 0, '2016-06-07', '2016-06-09', 54, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(41, 105, 12300453, 19, 8, 'lo que usted quiera', 'hola', 0, '2016-06-07', '2016-06-09', 85, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(42, 105, 11300539, 19, 8, 'lo que usted quiera', 'hola', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(43, 105, 12300172, 19, 8, 'lo que usted quiera', 'hola', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016102612_Hoja de cálculo sin título.xlsx', NULL, 1),
(258, 101, 12300156, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(46, 105, 12300023, 19, 8, 'sube un archivo', 'porfa', 0, '2016-06-07', '2016-06-14', -1, '', '1', 1),
(47, 105, 12300156, 19, 8, 'sube un archivo', 'porfa', 0, '2016-06-07', '2016-06-14', -1, '', '1', 1),
(48, 105, 12300239, 19, 8, 'sube un archivo', 'porfa', 1, '2016-06-07', '2016-06-14', 100, '', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_752016111451_CRONOGRAMA.docx', 1),
(49, 105, 12300266, 19, 8, 'sube un archivo', 'porfa', 0, '2016-06-07', '2016-06-14', -1, '', '1', 1),
(50, 105, 12300379, 19, 8, 'sube un archivo', 'porfa', 0, '2016-06-07', '2016-06-14', -1, '', '1', 1),
(51, 105, 12300392, 19, 8, 'sube un archivo', 'porfa', 0, '2016-06-07', '2016-06-14', -1, '', '1', 1),
(52, 105, 12300443, 19, 8, 'sube un archivo', 'porfa', 0, '2016-06-07', '2016-06-14', -1, '', '1', 1),
(53, 105, 12300453, 19, 8, 'sube un archivo', 'porfa', 0, '2016-06-07', '2016-06-14', -1, '', '1', 1),
(54, 105, 11300539, 19, 8, 'sube un archivo', 'porfa', 0, '2016-06-07', '2016-06-14', -1, '', '1', 1),
(55, 105, 12300172, 19, 8, 'sube un archivo', 'porfa', 0, '2016-06-07', '2016-06-14', -1, '', '1', 1),
(56, 101, 12300023, 16, 8, 'nueva tarea', 'hola', 0, '2016-06-07', '2016-06-09', 50, '', '1', 1),
(57, 101, 12300156, 16, 8, 'nueva tarea', 'hola', 0, '2016-06-07', '2016-06-09', -1, '', '1', 1),
(58, 101, 12300239, 16, 8, 'nueva tarea', 'hola', 1, '2016-06-07', '2016-06-09', 100, '', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_752016113315_doc1.docx', 1),
(59, 101, 12300266, 16, 8, 'nueva tarea', 'hola', 0, '2016-06-07', '2016-06-09', -1, '', '1', 1),
(60, 101, 12300379, 16, 8, 'nueva tarea', 'hola', 0, '2016-06-07', '2016-06-09', -1, '', '1', 1),
(61, 101, 12300392, 16, 8, 'nueva tarea', 'hola', 0, '2016-06-07', '2016-06-09', -1, '', '1', 1),
(62, 101, 12300443, 16, 8, 'nueva tarea', 'hola', 0, '2016-06-07', '2016-06-09', -1, '', '1', 1),
(63, 101, 12300453, 16, 8, 'nueva tarea', 'hola', 0, '2016-06-07', '2016-06-09', -1, '', '1', 1),
(64, 101, 11300539, 16, 8, 'nueva tarea', 'hola', 0, '2016-06-07', '2016-06-09', -1, '', '1', 1),
(65, 101, 12300172, 16, 8, 'nueva tarea', 'hola', 0, '2016-06-07', '2016-06-09', -1, '', '1', 1),
(66, 101, 12300023, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(67, 101, 12300156, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(68, 101, 12300239, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(69, 101, 12300266, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(70, 101, 12300379, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(71, 101, 12300392, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(72, 101, 12300443, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(73, 101, 12300453, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(74, 101, 11300539, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(75, 101, 12300172, 16, 8, 'Examen', 'estudiar sistemas expertos', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(76, 105, 12300023, 19, 8, 'nueva tarea', 'estudien', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', '1', 1),
(77, 105, 12300156, 19, 8, 'nueva tarea', 'estudien', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', '1', 1),
(78, 105, 12300239, 19, 8, 'nueva tarea', 'estudien', 1, '2016-06-07', '2016-06-09', 100, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_752016114510_doc1.docx', 1),
(79, 105, 12300266, 19, 8, 'nueva tarea', 'estudien', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', '1', 1),
(80, 105, 12300379, 19, 8, 'nueva tarea', 'estudien', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', '1', 1),
(81, 105, 12300392, 19, 8, 'nueva tarea', 'estudien', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', '1', 1),
(82, 105, 12300443, 19, 8, 'nueva tarea', 'estudien', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', '1', 1),
(83, 105, 12300453, 19, 8, 'nueva tarea', 'estudien', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', '1', 1),
(84, 105, 11300539, 19, 8, 'nueva tarea', 'estudien', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', '1', 1),
(85, 105, 12300172, 19, 8, 'nueva tarea', 'estudien', 0, '2016-06-07', '2016-06-09', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016114415_Banco de Preguntas.docx', '1', 1),
(86, 105, 12300023, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(87, 105, 12300156, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(88, 105, 12300239, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(89, 105, 12300266, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(90, 105, 12300379, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(91, 105, 12300392, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(92, 105, 12300443, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(93, 105, 12300453, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(94, 105, 11300539, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(95, 105, 12300172, 19, 8, 'Examen', 'PLC', 0, '2016-06-07', '2016-06-17', -1, '', NULL, 1),
(96, 101, 12300023, 16, 8, 'sergio tarea', 'hola', 0, '2016-06-07', '2016-06-14', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', '1', 1),
(97, 101, 12300156, 16, 8, 'sergio tarea', 'hola', 0, '2016-06-07', '2016-06-14', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', '1', 1),
(98, 101, 12300239, 16, 8, 'sergio tarea', 'hola', 1, '2016-06-07', '2016-06-14', 100, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_752016115241_CRONOGRAMA.docx', 1),
(99, 101, 12300266, 16, 8, 'sergio tarea', 'hola', 0, '2016-06-07', '2016-06-14', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', '1', 1),
(100, 101, 12300379, 16, 8, 'sergio tarea', 'hola', 0, '2016-06-07', '2016-06-14', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', '1', 1),
(101, 101, 12300392, 16, 8, 'sergio tarea', 'hola', 0, '2016-06-07', '2016-06-14', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', '1', 1),
(102, 101, 12300443, 16, 8, 'sergio tarea', 'hola', 0, '2016-06-07', '2016-06-14', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', '1', 1),
(103, 101, 12300453, 16, 8, 'sergio tarea', 'hola', 0, '2016-06-07', '2016-06-14', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', '1', 1),
(104, 101, 11300539, 16, 8, 'sergio tarea', 'hola', 0, '2016-06-07', '2016-06-14', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', '1', 1),
(105, 101, 12300172, 16, 8, 'sergio tarea', 'hola', 0, '2016-06-07', '2016-06-14', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115150_resumen-prog-offline.doc', '1', 1),
(106, 103, 12300239, 1, 0, 'nurca', 'yometi', 1, '2016-06-07', '2016-06-08', -1, '', '', 1),
(107, 101, 12300023, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(108, 101, 12300156, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(109, 101, 12300239, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(110, 101, 12300266, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(111, 101, 12300379, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(112, 101, 12300392, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(113, 101, 12300443, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(114, 101, 12300453, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(115, 101, 11300539, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(116, 101, 12300172, 16, 8, 'Examen', 'estudien', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(117, 101, 12300023, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', '1', 1),
(118, 101, 12300156, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', '1', 1),
(119, 101, 12300239, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_75201612026_CRONOGRAMA.docx', 1),
(120, 101, 12300266, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', '1', 1),
(121, 101, 12300379, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', '1', 1),
(122, 101, 12300392, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', '1', 1),
(123, 101, 12300443, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', '1', 1),
(124, 101, 12300453, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', '1', 1),
(125, 101, 11300539, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', '1', 1),
(126, 101, 12300172, 16, 8, 'química', 'que hay', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016115939_Banco de Preguntas.docx', '1', 1),
(128, 105, 12300491, 19, 9, 'Examen', 'hola\n', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(129, 105, 12300455, 19, 9, 'Examen', 'hola\n', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(130, 105, 12300494, 19, 9, 'Examen', 'hola\n', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(131, 105, 12300496, 19, 9, 'Examen', 'hola\n', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(132, 101, 12300023, 16, 8, 'pong', 'juego', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', '1', 1),
(133, 101, 12300156, 16, 8, 'pong', 'juego', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', '1', 1),
(134, 101, 12300239, 16, 8, 'pong', 'juego', 1, '2016-06-07', '2016-06-10', 18, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_752016122152_CRONOGRAMA.docx', 1),
(135, 101, 12300266, 16, 8, 'pong', 'juego', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', '1', 1),
(136, 101, 12300379, 16, 8, 'pong', 'juego', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', '1', 1),
(137, 101, 12300392, 16, 8, 'pong', 'juego', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', '1', 1),
(138, 101, 12300443, 16, 8, 'pong', 'juego', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', '1', 1),
(139, 101, 12300453, 16, 8, 'pong', 'juego', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', '1', 1),
(140, 101, 11300539, 16, 8, 'pong', 'juego', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', '1', 1),
(141, 101, 12300172, 16, 8, 'pong', 'juego', 0, '2016-06-07', '2016-06-10', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016122042_Banco de Preguntas.docx', '1', 1),
(142, 101, 12300023, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(143, 101, 12300156, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(144, 101, 12300239, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(145, 101, 12300266, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(146, 101, 12300379, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(147, 101, 12300392, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(148, 101, 12300443, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(149, 101, 12300453, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(150, 101, 11300539, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(151, 101, 12300172, 16, 8, 'Examen', 'cocoexamen', 0, '2016-06-07', '2016-06-09', -1, '', NULL, 1),
(162, 101, 12300023, 16, 8, 'expo', 'presentar', 0, '2016-06-07', '2016-06-08', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', '1', 1),
(163, 101, 12300156, 16, 8, 'expo', 'presentar', 0, '2016-06-07', '2016-06-08', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', '1', 1),
(164, 101, 12300239, 16, 8, 'expo', 'presentar', 1, '2016-06-07', '2016-06-08', 100, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_75201613658_CRONOGRAMA.docx', 1),
(165, 101, 12300266, 16, 8, 'expo', 'presentar', 0, '2016-06-07', '2016-06-08', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', '1', 1),
(166, 101, 12300379, 16, 8, 'expo', 'presentar', 0, '2016-06-07', '2016-06-08', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', '1', 1),
(167, 101, 12300392, 16, 8, 'expo', 'presentar', 0, '2016-06-07', '2016-06-08', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', '1', 1),
(168, 101, 12300443, 16, 8, 'expo', 'presentar', 0, '2016-06-07', '2016-06-08', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', '1', 1),
(169, 101, 12300453, 16, 8, 'expo', 'presentar', 0, '2016-06-07', '2016-06-08', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', '1', 1),
(170, 101, 11300539, 16, 8, 'expo', 'presentar', 0, '2016-06-07', '2016-06-08', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', '1', 1),
(171, 101, 12300172, 16, 8, 'expo', 'presentar', 0, '2016-06-07', '2016-06-08', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613557_Análisis de Mercado.pptx', '1', 1),
(172, 103, 12300239, 14, 0, 'mate6', 'diferencuales', 1, '2016-06-07', '2016-06-08', -1, '', '', 1),
(173, 113, 12300491, 23, 9, 'nuevo expo', 'REDES', 1, '2016-06-07', '2016-06-30', 100, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016131556_Banco de Preguntas.docx', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300491_752016131729_Reporte final.docx', 1),
(174, 113, 12300455, 23, 9, 'nuevo expo', 'REDES', 0, '2016-06-07', '2016-06-30', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016131556_Banco de Preguntas.docx', '1', 1),
(175, 113, 12300494, 23, 9, 'nuevo expo', 'REDES', 0, '2016-06-07', '2016-06-30', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016131556_Banco de Preguntas.docx', '1', 1),
(176, 113, 12300496, 23, 9, 'nuevo expo', 'REDES', 0, '2016-06-07', '2016-06-30', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016131556_Banco de Preguntas.docx', '1', 1),
(177, 103, 12300491, 22, 0, 'reporte wan', 'hacer un reporte', 1, '2016-06-07', '2016-06-22', -1, '', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300491_752016132018_Hoja de cálculo sin título.xlsx', 1),
(178, 113, 12300491, 23, 9, 'Examen', 'vvvb', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(179, 113, 12300455, 23, 9, 'Examen', 'vvvb', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(180, 113, 12300494, 23, 9, 'Examen', 'vvvb', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(181, 113, 12300496, 23, 9, 'Examen', 'vvvb', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(182, 113, 12300491, 23, 9, 'Examen', 'vbjig', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(183, 113, 12300455, 23, 9, 'Examen', 'vbjig', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(184, 113, 12300494, 23, 9, 'Examen', 'vbjig', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(185, 113, 12300496, 23, 9, 'Examen', 'vbjig', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(186, 113, 12300491, 23, 9, 'Examen', 'estudien todo', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(187, 113, 12300455, 23, 9, 'Examen', 'estudien todo', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(188, 113, 12300494, 23, 9, 'Examen', 'estudien todo', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(189, 113, 12300496, 23, 9, 'Examen', 'estudien todo', 0, '2016-06-07', '2016-06-16', -1, '', NULL, 1),
(190, 105, 12300023, 19, 8, 'exposición al grupi', 'hola', 0, '2016-06-07', '2016-06-17', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', '1', 1),
(191, 105, 12300156, 19, 8, 'exposición al grupi', 'hola', 0, '2016-06-07', '2016-06-17', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', '1', 1),
(192, 105, 12300239, 19, 8, 'exposición al grupi', 'hola', 1, '2016-06-07', '2016-06-17', 100, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_752016134016_Hoja de cálculo sin título.xlsx', 1),
(193, 105, 12300266, 19, 8, 'exposición al grupi', 'hola', 0, '2016-06-07', '2016-06-17', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', '1', 1),
(194, 105, 12300379, 19, 8, 'exposición al grupi', 'hola', 0, '2016-06-07', '2016-06-17', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', '1', 1),
(195, 105, 12300392, 19, 8, 'exposición al grupi', 'hola', 0, '2016-06-07', '2016-06-17', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', '1', 1),
(196, 105, 12300443, 19, 8, 'exposición al grupi', 'hola', 0, '2016-06-07', '2016-06-17', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', '1', 1),
(197, 105, 12300453, 19, 8, 'exposición al grupi', 'hola', 0, '2016-06-07', '2016-06-17', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', '1', 1),
(198, 105, 11300539, 19, 8, 'exposición al grupi', 'hola', 0, '2016-06-07', '2016-06-17', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', '1', 1),
(199, 105, 12300172, 19, 8, 'exposición al grupi', 'hola', 0, '2016-06-07', '2016-06-17', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/75201613392_Banco de Preguntas.docx', '1', 1),
(200, 103, 12300491, 21, 0, 'nyevs', 'y jafjjf', 0, '2016-06-07', '2016-06-15', -1, '', '', 1),
(292, 101, 12300239, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(293, 101, 12300266, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(290, 101, 12300023, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(291, 101, 12300156, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(289, 101, 12300172, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 0, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', '1', 1),
(288, 101, 11300539, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 0, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', '1', 1),
(287, 101, 12300453, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 0, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', '1', 1),
(286, 101, 12300443, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 0, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', '1', 1),
(285, 101, 12300392, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 0, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', '1', 1),
(284, 101, 12300379, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 0, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', '1', 1),
(283, 101, 12300266, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 0, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', '1', 1),
(282, 101, 12300239, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 1, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_247201691210_CRONOGRAMA.docx', 1),
(211, 101, 12300023, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(212, 101, 12300156, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(213, 101, 12300239, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(214, 101, 12300266, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(215, 101, 12300379, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(216, 101, 12300392, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(217, 101, 12300443, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(218, 101, 12300453, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(219, 101, 11300539, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(220, 101, 12300172, 16, 8, 'Examen', 'estudien mucho', 0, '2016-06-07', '2016-06-23', -1, '', NULL, 1),
(221, 103, 12300239, 15, 0, 'mate6', 'derivadas', 1, '2016-06-07', '2016-06-08', -1, '', 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/12300239_75201614125_Hoja de cálculo sin título.xlsx', 1),
(300, 103, 12300239, 15, 0, 'hhh', 'vhh', 0, '2016-08-24', '2016-08-31', -1, '', '', 1),
(298, 101, 11300539, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(299, 101, 12300172, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(294, 101, 12300379, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(295, 101, 12300392, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(296, 101, 12300443, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(297, 101, 12300453, 16, 8, 'Examen', 'estudisnsksos', 0, '2016-08-24', '2016-09-08', -1, '', NULL, 1),
(232, 101, 12300023, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(233, 101, 12300156, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(234, 101, 12300239, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(235, 101, 12300266, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(236, 101, 12300379, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(237, 101, 12300392, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(238, 101, 12300443, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(239, 101, 12300453, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(240, 101, 11300539, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(241, 101, 12300172, 16, 8, 'Examen', 'estudien PLC\n', 0, '2016-06-07', '2016-06-30', -1, '', NULL, 1),
(242, 101, 12300491, 16, 9, 'nueca', 'cuenta', 1, '2016-06-07', '2016-07-27', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016141117_resumen-prog-offline.doc', NULL, 1),
(243, 101, 12300455, 16, 9, 'nueca', 'cuenta', 0, '2016-06-07', '2016-07-27', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016141117_resumen-prog-offline.doc', NULL, 1),
(244, 101, 12300494, 16, 9, 'nueca', 'cuenta', 0, '2016-06-07', '2016-07-27', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016141117_resumen-prog-offline.doc', NULL, 1),
(245, 101, 12300496, 16, 9, 'nueca', 'cuenta', 0, '2016-06-07', '2016-07-27', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/752016141117_resumen-prog-offline.doc', NULL, 1),
(246, 101, 12300023, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(247, 101, 12300156, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(248, 101, 12300239, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(249, 101, 12300266, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(250, 101, 12300379, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(251, 101, 12300392, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(252, 101, 12300443, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(253, 101, 12300453, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(254, 101, 11300539, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(255, 101, 12300172, 16, 8, 'Examen', 'que hay perros', 0, '2016-06-08', '2016-06-23', -1, '', NULL, 1),
(256, 103, 12300379, 19, 0, 'mia', 'mía de mi', 0, '2016-06-08', '2016-06-09', -1, '', '', 1),
(257, 101, 12300023, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(259, 101, 12300239, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(260, 101, 12300266, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(261, 101, 12300379, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(262, 101, 12300392, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(263, 101, 12300443, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(264, 101, 12300453, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(265, 101, 11300539, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(266, 101, 12300172, 16, 8, 'preguntas', 'ganancias de flappy bird\ncomparación entre ganancias de appstore y google play store\ncontrol de calidad de google play store\n', 0, '2016-06-13', '2016-06-14', -1, '', NULL, 1),
(267, 101, 12300023, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(268, 101, 12300156, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(269, 101, 12300239, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(270, 101, 12300266, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(271, 101, 12300379, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(272, 101, 12300392, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(273, 101, 12300443, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(274, 101, 12300453, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(275, 101, 11300539, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(276, 101, 12300172, 16, 8, 'gato', 'en 2 celulares\ncon historial\nfacil de usar\ndiagrama de interaccion\ncomo se instala\nmanual de usuario\ny no se que mad', 0, '2016-06-17', '2016-06-21', -1, '', NULL, 1),
(281, 101, 12300156, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 0, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', '1', 1),
(280, 101, 12300023, 16, 8, 'hagan un resumen', 'mjsjzjsjsj', 0, '2016-08-24', '2016-08-31', -1, 'http://schoolapphost.hol.es/ArchivosPrueba/uploads/24720169950_doc3.doc', '1', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE IF NOT EXISTS `trabajador` (
  `nomina` int(11) NOT NULL AUTO_INCREMENT,
  `FK_area` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido_paterno` varchar(30) NOT NULL,
  `apellido_materno` varchar(30) NOT NULL,
  `puesto` varchar(50) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `password` varchar(50) NOT NULL,
  `palabra_clave` varchar(50) NOT NULL,
  `idReg` text NOT NULL,
  PRIMARY KEY (`nomina`),
  KEY `FK_area` (`FK_area`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Volcado de datos para la tabla `trabajador`
--

INSERT INTO `trabajador` (`nomina`, `FK_area`, `nombre`, `apellido_paterno`, `apellido_materno`, `puesto`, `estado`, `password`, `palabra_clave`, `idReg`) VALUES
(99, 3, 'admin', 'admin', 'admin1', 'administrador', 1, '21232f297a57a5a743894a0e4a801fc3', 'admin', 'dIBIqAgx7xs:APA91bEmf2_nq5S3-ySNZr9qCo8mjaxKnXxMrwqIDkSB3jAKMctoTlfoKPKP6eo42eqZ0JO-fdIM_HYlqe1I9NUUEvFpVln1F1XqmIpkLaxfpl-epMy-fhc2F77aFl8fOuceQ-A5qzrd'),
(116, 3, 'Miguel', 'Rodriguez', 'Mora', 'subdirector', 1, '651faef175451b43088ed6fab4aab961', 'Mora', ''),
(101, 8, 'Ismael', 'Lopez', 'Buenrostro', 'maestro', 1, 'ac0ddf9e65d57b6a56b2453386cd5db5', 'coco', ''),
(102, 8, 'Carlos', 'Molina', 'Martinez', 'maestro', 1, '37777c30635fb8d5ef622d54b9e77f35', 'molina', ''),
(103, 3, 'tarea_alumno', 'tarea_alumno', 'tarea_alumno', 'tarea_alumno', 1, 'cf7b3a15836286add5631ec24aadb7d0', 'tarea_alumno', ''),
(104, 8, 'Sergio', 'Becerra', 'Delgado', 'maestro', 1, 'dc1d67d1a5e9d52940945516548c8ec3', 'sergio', ''),
(105, 8, 'Juan Manuel', 'Garcia', 'Leon', 'maestro', 1, '28096c42a5dfd87cdbca88efe2006f8f', 'Leon', ''),
(106, 8, 'Antonio', 'Gonzalez', 'Lozano', 'maestro', 1, '885383f16fccd10370814fcd862aa10d', 'Lozano', ''),
(107, 8, 'Jose Gustavo', 'Rojas', 'Garcia', 'maestro', 1, '2f455796ada823794f0e26c2680dee83', 'Garcia', ''),
(108, 3, 'Maria Angelita', 'Galan', 'Barragan', 'maestro', 1, '720d20d840d704b6c2d31f02d7301198', 'Barragan', ''),
(109, 3, 'Raul Ernesto', 'Martines', 'Chavez', 'maestro', 1, '63120a7a6f3c5b438d840c9c67cdaca9', 'Chavez', ''),
(110, 8, 'Carlos Alberto', 'Ramirez', 'Garcia', 'maestro', 1, '1aae7c5b797bab953a645fdf0febf474', 'Garcia', ''),
(111, 3, 'Misael', 'Rivera', 'Cortes', 'maestro', 1, '3330a4678e150937411f5276ae1d0236', 'Cortes', ''),
(112, 8, 'Andres', 'Figueroa', 'Flores', 'maestro', 1, '27d2abba75fc7ec93e9c625fe3af8467', 'Flores', ''),
(113, 8, 'Rodolfo Ulyses', 'Vazquez', 'Cardenas', 'maestro', 1, 'd38c9ac316d2b17c36c593751f91b7e8', 'Cardenas', ''),
(114, 3, 'Odette Catalina', 'Guzman', 'Perez', 'maestro', 1, '1f489f4f9c3fc1a7c581a7bd2326968c', 'Perez', ''),
(115, 3, 'Willibaldo', 'Ruiz', 'Arevalo', 'director', 1, 'e7236697824fb37763235980f1061218', 'Arevalo', ''),
(117, 20, 'Edgar', 'Montez', 'Ramirez', 'cadMaestro', 1, 'cf55ada960dedf6154b0d6905b2748ff', 'Ramirez', ''),
(118, 21, 'Carlo', 'Arceo', 'Castaneda', 'cadCoordinador', 1, '2a5a93caf31cbc3e2b9073d7e7c7dd70', 'Casta?eda', ''),
(119, 3, 'Eunice', 'Martin del Campo', 'Castaneda', 'goe', 1, 'c9199c53c2f321d9aee75aa99d4b74cc', 'Casta?eda', ''),
(120, 8, 'Mariana', 'Ruiz', 'Velazco', 'secretaria', 1, '5c886469358678ea7528ef60184a729e', 'Velazco', ''),
(121, 9, 'Melisa', 'Sandoval', 'Avalos', 'secretaria', 1, '78e890b985520b5d75f544bdc37599fb', 'Avalos', ''),
(122, 4, 'Moises', 'Padilla', 'Gomez', 'maestro', 1, 'a04546498a5468af668a0b7fc291dc9d', 'Gomez', ''),
(123, 3, 'Celso', 'Espinoza', 'Corona', 'director', 1, '77688fecdadfcb68fad8a85c3103e48c', 'Corona', ''),
(124, 8, 'Adrian', 'Gomez', 'Hernandez', 'coordinadorCarrera', 1, 'c1937b687795ce80be0ecf20e1811441', 'Hernandez', ''),
(125, 10, 'Alma Marcela', 'Silva', 'de Alegria', 'secretaria', 1, 'bcdf6a9ece0d46ac5751efe4bd4026d5', 'de Alegria', ''),
(126, 16, 'Ivan', 'Agraz', 'de Santiago', 'secretaria', 1, '036e3491ba499df64611c82b6bcdd37d', 'de Santiago', ''),
(127, 15, 'Raul', 'Partida', 'Iniguez', 'secretaria', 1, '3fb4b5eb20a39e30b55e2d387541ccd2', 'Iniguez', ''),
(128, 8, 'Gusa', 'Ferrer', 'Hernandez', 'maestro', 1, '1e702070054be113466bde67212f86d6', 'Hernandez', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
